from helper_functions import *

network_parameters = {

    # amount of neurons in the reservoir
    'reservoir_size' : 500,

    #
    'leaking_rate' : 0.005,

    'regulation_rate': 0.05,

    # precentage of null connections in the reservoir
    'reservoir_sparsity' : 0.5,

    # the mean of the generated weights in the reservoir
    'reservoir_connectivity_mean' : 0,

    # the variance of the generated weights in the reservoir
    'reservoir_connectivity_variance' : 1,

    # precentage of null connections from input to reservoir
    'input_sparsity' : 0.5,

    # the mean of the generated weights from input
    'input_connectivity_mean' : 0,

    # the variance of the generated weights from input
    'input_connectivity_variance' : 1,

    #
    'output_size': 100,

    "init_outweights_function": Xavier_init,

    "regul_rate": 0.03,

    "activation_function": np.tanh
}



