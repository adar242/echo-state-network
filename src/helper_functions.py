import numpy as np

def Xavier_init(reservoir_size, output_size):


    weights = np.random.normal(loc=0, scale=np.sqrt(2/(reservoir_size + output_size)), size=(output_size, reservoir_size))


    return weights