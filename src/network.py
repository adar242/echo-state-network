import numpy as np
import random

from hyper_parameters import *

# PARAMS the network hyper parameters
# return randomly generated weights for the input-to-reservoir and weights for the reservoir-to-reservoir
def init_network(network_parameters):
    reservoir_size = network_parameters["reservoir_size"]

    # generate weights in the reservoir
    reservoir_weights = np.random.normal(loc=network_parameters["reservoir_connectivity_mean"],
                                         scale=network_parameters["reservoir_connectivity_variance"],
                                         size=(reservoir_size, reservoir_size)
                                         )

    # generate random numbers from 1 upto (reservoir_size ** 2)
    amount_indices = reservoir_size ** 2
    amount_to_pick = int(network_parameters['reservoir_sparsity'] * amount_indices)
    zeroed_indices = random.sample(range(amount_indices), amount_to_pick)

    # zeroes some of the weights
    for indice in zeroed_indices:
        row = int(indice / reservoir_size)
        col = indice % reservoir_size

        reservoir_weights[row][col] = 0

    # divide by the maximum real eigen value (the spectral radius)
    real_max_value = np.max(np.real(np.linalg.eigvals(reservoir_weights)))
    reservoir_weights = (1 / real_max_value) * reservoir_weights



    # generating weights from input to the reservoir
    input_weights = np.random.normal(loc=network_parameters["input_connectivity_mean"],
                                     scale=network_parameters["input_connectivity_variance"],
                                     size=(reservoir_size, 1) # only one input
                                     )
    # zeroes some of the wights
    zeroed_indices = np.random.choice(np.arange(input_weights.size), replace=False,
                           size=int(input_weights.size * network_parameters["input_sparsity"]))
    input_weights[zeroed_indices] = 0


    # generating the output weights
    output_weights = network_parameters["init_outweights_function"](reservoir_size, network_parameters["output_size"])

    return input_weights, reservoir_weights, output_weights

# recives the input for thenetwork, current reservoir state, reservoir weights,input weights, leaking parameter, and the activation function
# returns the updated reservoir state
def update_reservoir(input,reservoir_state, reservoir_weights, input_weights, leaking_rate, activation_function):
    reservoir_state = ((1 - leaking_rate) * reservoir_state) + leaking_rate * activation_function(np.dot(input, input_weights) + np.dot(reservoir_state,reservoir_weights))
    return reservoir_state


def update_weights(reservoir_states, desired_outputs, regul_rate):
    states_corr_matrix = np.corrcoef(reservoir_states,reservoir_states)
    cross_corr_matrix = np.corrcoef(reservoir_states, desired_outputs)

    regul_matrix = np.identity(len(states_corr_matrix)) * regul_rate

    output_weights = np.linalg.inv(states_corr_matrix + regul_matrix) * cross_corr_matrix

    return output_weights


def generate_input():
    return np.random.randn()


def get_desired_outputs(time, inputs, output_size):
    desired_outputs = []
    for k in range(output_size):
        if time - k < 0:
            desired_outputs.append(0)
        else:
            desired_outputs.append(inputs[time - k])

    return desired_outputs

def train_network(batch_size, reservoir_weights, input_weights, network_parameters):
    inputs = []
    reservoir_states = []
    current_state = np.array([0] * network_parameters["reservoir_size"])
    for i in range(batch_size):
        input = generate_input()
        inputs.append(input)

        current_state = update_reservoir(input, current_state, reservoir_weights, input_weights, network_parameters["leaking_rate"], network_parameters["activation_function"])
        reservoir_states.append(current_state)
        desired_outputs = get_desired_outputs(i, inputs, network_parameters["output_size"])


